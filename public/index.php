<?php

declare(strict_types=1);

use Core\Application;
use Narrowspark\HttpEmitter\SapiEmitter;

require __DIR__ . '/../vendor/autoload.php';

/**
 * Middlewares
 */
$res = \Middlewares\Utils\Dispatcher::run([
    function($request, $next) {
        return $next->handle($request);
    }
]);

$app = new Application();

$emitter = new SapiEmitter();
return $emitter->emit($app->run());
