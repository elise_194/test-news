<?php

namespace App\Controllers\admin;

use App\Models\News;
use App\Services\NewsService;
use Core\Controller;
use Core\Pagination;
use Core\Request;
use Exception;
use Zend\Diactoros\Response\RedirectResponse;

/**
 * Class NewsController.
 *
 * @package App\Controllers\admin
 */
class NewsController extends Controller
{
    /**
     * Index page.
     *
     * @return mixed
     * @throws Exception
     */
    public function index()
    {
        $pagination = new Pagination(new News(), '/admin/news');
        $news = $pagination->models;

        return $this->render('admin/news/index.twig', [
            'news' => $news,
            'pagination' => $pagination,
        ]);
    }

    /**
     * Creating new model
     *
     * @return mixed
     * @throws Exception
     */
    public function create()
    {
        $model = new News();
        $request = Request::getInstance();
        if ($request->isPost()) {
            try {
                $service = new NewsService();
                $service->create($request);
                $msg = ['status' => 'success', 'text' => 'Successfully created!'];
            } catch (\Exception $e) {
                $msg = ['status' => 'error', 'text' => $e->getMessage()];
            }
        }

        return $this->render('admin/news/create.twig', [
            'model' => $model,
            'msg' => $msg
        ]);
    }

    /**
     * Updating model by id
     *
     * @return mixed
     * @throws Exception
     */
    public function edit()
    {
        $request = Request::getInstance();
        $model = (new News)->findOne($request->id);
        if ($request->isPost()) {
            try {
                $service = new NewsService();
                $service->update($model, $request);
                $msg = ['status' => 'success', 'text' => 'Successfully updated!'];
            } catch (\Exception $e) {
                $msg = ['status' => 'error', 'text' => $e->getMessage()];
            }
        }

        return $this->render('admin/news/update.twig', [
            'model' => $model,
            'msg' => $msg
        ]);
    }

    /**
     * Deletes model
     *
     * @return RedirectResponse
     */
    public function delete()
    {
        $request = Request::getInstance();
        $model = (new News)->findOne($request->id);
        $model->delete();

        return $this->redirect('/admin/news');
    }
}
