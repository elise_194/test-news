<?php

namespace App\Controllers;

use App\Models\News;
use Core\Controller;
use Core\Pagination;
use Core\Request;
use Psr\Http\Message\ResponseInterface;
use Exception;

/**
 * Class SiteController
 * @package App\Controllers
 *
 * @property ResponseInterface $response
 */
class SiteController extends Controller
{
    /**
     * Index page.
     *
     * @return mixed
     * @throws Exception
     */
    public function index()
    {
        $pagination = new Pagination(new News(), '/');
        $news = $pagination->models;

        return $this->render('Site/index.twig', [
            'news' => $news,
            'pagination' => $pagination
        ]);
    }

    /**
     * @throws \Exception
     */
    public function view()
    {
        $request = Request::getInstance();
        $model = (new News)->findOne($request->id);
        $other = (new News)->findAll('limit 10');

        return $this->render('Site/view.twig', [
            'model' => $model,
            'other' => $other,
        ]);
    }
}
