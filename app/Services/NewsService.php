<?php

namespace App\Services;

use App\Models\News;
use Core\Request;
use Exception;

/**
 * Class NewsService.
 *
 * @package App\Services
 */
class NewsService
{
    /**
     * Creating news
     *
     * @param Request $request
     * @return News
     * @throws Exception
     */
    public function create(Request $request)
    {
        $content = new News();
        $content->title = $request->title;
        $content->description = $request->description;
        $content->content = $request->content;
        if (! $content->save()) {
            throw new Exception(implode("<br>", $content->errors));
        }

        return $content;
    }

    /**
     * Updating model
     *
     * @param News $content
     * @param Request $request
     * @return mixed
     * @throws Exception
     */
    public function update($content, Request $request)
    {
        $content->title = $request->title;
        $content->description = $request->description;
        $content->content = $request->content;
        if (! $content->save()) {
            throw new Exception(implode("<br>", $content->errors));
        }

        return $content;
    }
}
