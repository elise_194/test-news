<?php

namespace App\Models;

use Core\Model;

/**
 * Class News.
 * @package App\Models
 *
 * @property int $id
 * @property string $title
 * @property string $description
 * @property string $content
 * @property string $created_at
 * @property string $image
 */
class News extends Model
{
    /**
     * @return string
     */
    public static function tableName()
    {
        return 'news';
    }

    /**
     * @return array
     */
    public function rules()
    {
        return [
            ['required', ['title', 'description', 'content']],
            ['string', ['title', 'description']],
            ['string', ['content'], 1000],
        ];
    }
}
