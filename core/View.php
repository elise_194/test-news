<?php

namespace Core;

use Twig\Environment;
use Twig\Loader\FilesystemLoader;

/**
 * Class View
 * @package App\Core
 */
class View
{
    /**
     * @param $view
     * @param array $args
     * @throws \Exception
     */
    public static function render($view, $args = [])
    {
        extract($args, EXTR_SKIP);
        $file = dirname(__DIR__) . "app/Views/$view";

        if (is_readable($file)) {
            require $file;
        } else {
            throw new \Exception("view file {$file} not found");
        }
    }

    /**
     * @param $template
     * @param array $args
     * @throws \Twig\Error\LoaderError
     * @throws \Twig\Error\RuntimeError
     * @throws \Twig\Error\SyntaxError
     */
    public static function renderTemplate($template, $args = [])
    {
        static $twig = null;

        if ($twig == null) {
            $loader = new FilesystemLoader(dirname(__DIR__) . '/app/Views');
            $twig = new Environment($loader);
        }

        return $twig->render($template, $args);
    }
}
