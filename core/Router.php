<?php

namespace Core;

use DI\Container;
use FastRoute\Dispatcher;
use Zend\Diactoros\Response;

/**
 * Class Router
 * @package App\Core
 *
 * @property $routes
 * @property $dispatcher
 */
class Router
{
    /**
     * @var $routes
     */
    protected $routes = [];

    /**
     * @var $dispatcher
     */
    protected $dispatcher;

    /**
     * Router constructor.
     */
    public function __construct()
    {
        $this->loadRoutes();
    }

    /**
     * @param Container $container
     * @return Response|null
     * @throws \DI\DependencyException
     * @throws \DI\NotFoundException
     */
    public function run(Container $container)
    {
        $response = null;
        $uri = $this->getUri();
        $routeInfo = $this->dispatcher->dispatch($this->getHttpMethod(), $uri);
        switch ($routeInfo[0]) {
            case Dispatcher::NOT_FOUND:
                $response = new Response('404', 400);
                $response->getBody()->write('404');
                break;
            case Dispatcher::METHOD_NOT_ALLOWED:
                $allowedMethods = $routeInfo[1];
                $response = new Response('405', 405);
                $response->getBody()->write('405');
                break;
            case Dispatcher::FOUND:
                $handler = $routeInfo[1];
                $vars = $routeInfo[2];
                list($class, $method) = explode('@',$handler,2);
                $controller = $container->get($class);
                $response = $controller->{$method}(...array_values($vars));
                break;
        }

        return $response;
    }

    /**
     * Load routes from routes.php
     */
    protected function loadRoutes(): void
    {
        $this->dispatcher = require_once __DIR__ . '/../config/routes.php';
    }

    /**
     * @return mixed
     */
    protected function getHttpMethod()
    {
        return $_SERVER['REQUEST_METHOD'];
    }

    /**
     * @return string
     */
    protected function getUri()
    {
        $uri = $_SERVER['REQUEST_URI'];

        if (false !== $pos = strpos($uri, '?')) {
            $uri = substr($uri, 0, $pos);
        }
        return rawurldecode($uri);
    }
}
