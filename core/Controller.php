<?php

namespace Core;

use Psr\Http\Message\ResponseInterface;
use Zend\Diactoros\Response\RedirectResponse;

/**
 * Class Controller
 * @package App\Controllers
 *
 * @property ResponseInterface $response
 */
abstract class Controller
{
    /**
     * @var object|ResponseInterface
     */
    protected object $response;

    /**
     * Controller constructor.
     * @param ResponseInterface $response
     */
    public function __construct(ResponseInterface $response)
    {
        $this->response = $response;
    }

    /**
     * @param $template
     * @param array $args
     * @return mixed
     * @throws \Twig\Error\LoaderError
     * @throws \Twig\Error\RuntimeError
     * @throws \Twig\Error\SyntaxError
     */
    public function render($template, $args = [])
    {
        $response = $this->response->withHeader('Content-Type', 'text/html');
        $response->getBody()
            ->write(View::renderTemplate($template, $args));
        return $response;
    }

    /**
     * @param $url
     * @return RedirectResponse
     */
    public function redirect($url)
    {
        return new RedirectResponse($url);
    }
}
