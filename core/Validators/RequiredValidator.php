<?php

namespace Core\Validators;

use Core\Validator;

/**
 * Class RequiredValidator.
 *
 * @package Core\Validators
 */
class RequiredValidator extends Validator
{
    /**
     * Validates attribute
     *
     * @param $model
     * @param $attribute
     */
    public function validateAttribute($model, $attribute)
    {
        $value = $model->$attribute;
        if (empty($value)) {
            $this->addError($model, $attribute, "{$attribute} is required");

            return;
        }
    }
}
