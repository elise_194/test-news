<?php

namespace Core\Validators;

use Core\Validator;

/**
 * Class StringValidator.
 *
 * @package Core\Validators
 */
class StringValidator extends Validator
{
    public $max = 255;

    /**
     * StringValidator constructor.
     *
     * @param $attributes
     * @param $max
     */
    public function __construct($attributes, $max)
    {
        if ($max) {
            $this->max = $max;
        }

        parent::__construct($attributes);
    }

    /**
     * Validates attribute
     *
     * @param $model
     * @param $attribute
     */
    public function validateAttribute($model, $attribute)
    {
        $value = $model->$attribute;
        if (!is_string($value)) {
            $this->addError($model, $attribute, "{$attribute} must be a string");

            return;
        }

        $length = mb_strlen($value,  'UTF-8');
        if ($length < 1) {
            $this->addError($model, $attribute, "{$attribute} too short");
        }

        if ($length > $this->max) {
            $this->addError($model, $attribute, "{$attribute} too long");
        }
    }
}
