<?php

namespace Core;

use App\Controllers\admin\NewsController;
use App\Controllers\SiteController;
use DI\Container;
use Dotenv\Dotenv;
use DI\ContainerBuilder;
use Zend\Diactoros\Response;
use function DI\create;
use function DI\get;
use \RedBeanPHP\R;

/**
 * Class Application
 * @package App\Core
 *
 * @property Container $container
 * @property Router $router
 */
class Application
{
    /**
     * @var Container $container
     */
    protected Container $container;

    /**
     * @var Router $router
     */
    protected Router $router;

    /**
     * Application constructor.
     */
    public function __construct()
    {
        $this->buildContainer();
        $this->loadEnv();
        $this->router = new Router();
        $this->initConnection();
        $this->initRequest();
    }

    /**
     * @return Container
     */
    public function getContainer()
    {
        return $this->container;
    }

    public function run()
    {
        return $this->router->run($this->getContainer());
    }

    /**
     * Create app container and load definitions
     */
    protected function buildContainer(): void
    {
        $containerBuilder = new ContainerBuilder();
        $containerBuilder->useAutowiring(false);
        $containerBuilder->useAnnotations(false);
        $containerBuilder->addDefinitions([
            SiteController::class => create(\App\Controllers\SiteController::class)
                ->constructor(get('Response')),
            'Response' => function() {
                return new Response();
            }
        ], [
            NewsController::class => create(\App\Controllers\admin\NewsController::class)
                ->constructor(get('Response')),
            'Response' => function() {
                return new Response();
            }
        ]);

        /** @noinspection PhpUnhandledExceptionInspection */
        $this->container = $containerBuilder->build();
    }

    /**
     * Load env settings
     */
    protected function loadEnv()
    {
        $dotenv = Dotenv::createImmutable(dirname(__DIR__));
        $dotenv->load();
    }

    /**
     * Init database connection.
     */
    protected function initConnection()
    {
        R::setup( "mysql:host=" . getenv('DATABASE_HOST')
            . ';dbname=' . getenv('DATABASE_NAME'),
            getenv('DATABASE_USER'), getenv('DATABASE_PASS')
        );
    }

    /**
     * Init request
     */
    public function initRequest()
    {
        $request = Request::getInstance();
        $request->setRequest();
    }
}
