<?php

namespace Core;

use Core\Interfaces\ValidatorInterface;

/**
 * Class Validator.
 *
 * @package Core
 */
abstract class Validator implements ValidatorInterface
{
    public $attributes;

    public static $validators = [
        'string' => 'Core\Validators\StringValidator',
        'required' => 'Core\Validators\RequiredValidator',
    ];

    /**
     * StringValidator constructor.
     * @param $attributes
     */
    public function __construct($attributes)
    {
        $this->attributes = $attributes;
    }

    /**
     * @param $type
     * @param $attributes
     * @param $params
     * @return mixed
     */
    public static function createValidator($type, $attributes, $params)
    {
        return new self::$validators[$type]($attributes, $params);
    }

    /**
     * Validates all attributes by rule
     *
     * @param $model
     */
    public function validateAttributes($model)
    {
        foreach ($this->attributes as $attribute) {
            $this->validateAttribute($model, $attribute);
        }
    }

    /**
     * @param Model $model
     * @param $attribute
     * @param $error
     */
    public function addError(Model $model, $attribute, $error)
    {
        $model->errors[$attribute] = $error;
    }
}
