<?php

namespace Core;

/**
 * Class Request.
 *
 * @package Core
 */
class Request
{
    public $request;

    private static $instances = [];

    protected function __construct()
    {
    }

    protected function __clone()
    {
    }

    public function __wakeup()
    {
    }

    public static function getInstance(): Request
    {
        $cls = static::class;
        if (!isset(self::$instances[$cls])) {
            self::$instances[$cls] = new static;
        }

        return self::$instances[$cls];
    }

    public function setRequest()
    {
        $this->request = $_REQUEST;
    }

    public function __get($name)
    {
        if (isset($this->request[$name])) {
            return $this->request[$name];
        }
    }

    /**
     * @return bool
     */
    public function isPost()
    {
        return !empty($_POST);
    }
}
