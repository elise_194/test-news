<?php

namespace Core;

/**
 * Class Pagination.
 *
 * @package Core
 */
class Pagination
{
    const
        PAGE_SIZE = 10,         // qnt rows per page
        MAX_QNT_PAGES = 10;     // qnt page links in pagination

    public $model;
    public $page;
    public $totalCount = 0;
    public $totalPages;
    public $models;
    public $firstPage;
    public $lastPage;
    public $url;

    /**
     * Pagination constructor.
     *
     * @param Model $model
     * @param $url
     */
    public function __construct(Model $model, $url)
    {
        $this->model = $model;
        $this->url = $url;
        $this->setPage();
        $this->setModels();
        $this->setTotalCount();
        $this->setTotalPages();
        $this->setLinks();
    }

    /**
     * Sets page number.
     */
    public function setPage()
    {
        $request = Request::getInstance();
        $this->page = $request->page ?? 1;
    }

    /**
     * Sets models from database.
     */
    public function setModels()
    {
        $this->models = $this->model->findAll(
            'order by id desc limit ' . self::PAGE_SIZE . ' offset ' . $this->getOffset()
        );
    }

    /**
     * Sets total count of found models.
     */
    public function setTotalCount()
    {
        $this->totalCount = count( $this->model->findAll());
    }

    /**
     * Sets the total qnt of pagination pages
     */
    public function setTotalPages()
    {
        $this->totalPages = ceil($this->totalCount / self::PAGE_SIZE);
    }

    /**
     * @return float|int
     */
    public function getOffset()
    {
        return ($this->page - 1) * self::PAGE_SIZE;
    }

    /**
     * Sets the first and last page to render in paginator
     */
    public function setLinks()
    {
        $half = intdiv(self::MAX_QNT_PAGES, 2);
        if ($this->page < $half || $this->totalPages <= self::MAX_QNT_PAGES) {
            $this->firstPage = 1;
            $this->lastPage = $this->totalPages < self::MAX_QNT_PAGES ? $this->totalPages : self::MAX_QNT_PAGES;
        } else {
            $this->firstPage = ($this->page - $half) > 0 ? $this->page - $half : 1 ;
            $this->lastPage = ($this->page + $half) <= $this->totalPages ? $this->page + $half : $this->totalPages;
        }
    }
}
