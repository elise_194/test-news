<?php

namespace Core\Interfaces;

/**
 * Interface ValidatorInterface.
 *
 * @package Core\Interfaces
 */
interface ValidatorInterface
{
    public function validateAttribute($model, $attribute);
}
