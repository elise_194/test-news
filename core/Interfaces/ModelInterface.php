<?php

namespace Core\Interfaces;

/**
 * Interface ModelInterface.
 *
 * @package Core\interfaces
 */
interface ModelInterface
{
    public static function tableName();
    public function rules();
}
