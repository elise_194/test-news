<?php

namespace Core;

use Core\Interfaces\ModelInterface;
use RedBeanPHP\R;
use RedBeanPHP\SimpleModel;
use RedBeanPHP\RedException\SQL;

/**
 * Class Model.
 *
 * @package Core
 */
abstract class Model extends SimpleModel implements ModelInterface
{
    public $errors;

    /**
     * Model constructor.
     */
    public function __construct()
    {
        $this->bean = R::dispense(static::tableName());
    }

    /**
     * Saves model to database
     *
     * @return int|string|bool
     * @throws SQL
     */
    public function save()
    {
        if (!$this->validate()) {
            return false;
        }

        return R::store($this->bean);
    }

    /**
     * Delets model
     */
    public function delete()
    {
        R::trash($this->bean);
    }

    /**
     * Finds all rows from table.
     *
     * @param null $condition
     * @return array
     */
    public function findAll($condition = null)
    {
        return R::findAll(static::tableName(), $condition);
    }

    /**
     * Finds one row from database by id.
     *
     * @param int $id
     * @return $this
     */
    public function findOne($id)
    {
        $this->bean = R::findOne(static::tableName(), 'id = ' . $id);
        return $this;
    }

    /**
     * Validates attributes
     */
    public function validate()
    {
        /** @var Validator $validator */
        foreach ($this->getValidator() as $validator) {
            $validator->validateAttributes($this);
        }

        if (!empty($this->errors)){
            return false;
        }

        return true;
    }

    /**
     * Gets validators classes
     */
    public function getValidator()
    {
        $validators = [];
        foreach ($this->rules() as $rule) {
            $validators[] = Validator::createValidator($rule[0], $rule[1], $rule[2] ?? null);
        }

        return $validators;
    }
}
