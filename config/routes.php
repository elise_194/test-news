<?php
/**
 * Add routes
 */
return \FastRoute\simpleDispatcher(function(\FastRoute\RouteCollector $r) {
    $r->addRoute('GET', '/', 'App\Controllers\SiteController@index');
    $r->addRoute('GET', '/index', 'App\Controllers\SiteController@index');
    $r->addRoute('GET', '/view', 'App\Controllers\SiteController@view');
    $r->addRoute('GET', '/admin', 'App\Controllers\admin\NewsController@index');
    $r->addRoute('GET', '/admin/news', 'App\Controllers\admin\NewsController@index');
    $r->addRoute('GET', '/admin/news/delete', 'App\Controllers\admin\NewsController@delete');
    $r->addRoute(['GET', 'POST'], '/admin/news/create', 'App\Controllers\admin\NewsController@create');
    $r->addRoute(['GET', 'POST'], '/admin/news/edit', 'App\Controllers\admin\NewsController@edit');
});
